#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
export $(grep -v '^#' ${SCRIPT_DIR}/.env | xargs)
export $(grep -v '^#' ${SCRIPT_DIR}/.restic-env | xargs)

docker run --rm -ti \
    -e RESTIC_REPOSITORY=$RESTIC_REPOSITORY \
    -e RESTIC_PASSWORD=$RESTIC_PASSWORD \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    restic/restic snapshots


