#!/bin/bash

printf "Stopping Nextcloud and Onlyoffice...\n"
docker-compose down

printf "\nNextcloud and Onlyoffice now stopped.\n"