# Nextcloud with OnlyOffice

## Introduction

This repo provides a deployment of Nextcloud and OnlyOffice suitable for a small organisation.

## Create VPS and Install the Software

1. Set up a VPS with at least the following features:
    - 2GB RAM
    - 60GB HD
    - Linux 20.04 with root account
1. Point subdomain A record at the VPS public IP - it needs to work for certbot to do its job
1. Clone into default (root) directory
1. Copy `.env-example` to `.env` and fill in the values. Since these contain secrets the copied file should *not* be committed here, but saved in a password manager
1. CD into directory (nextcloud)
1. `bash ./install-docker.sh` 
1. `bash ./install-docker-compose.sh` 
1. `bash ./start.sh`

## Setup Nextcloud

Full documentation is available at https://nextcloud.com, but I suggest the following:

1. Navigate to address and enter user name and password for admin account 
1. Navigate to apps and install, for example:
    - (Auditing/Logging)
    - Calendar
    - Contacts
    - Mail
    - Talk
    - Deck
    - Maps
    - Drawio
    - Announcement center
1. Set up mail:
    - Set up your mail address in Settings | Personal | Personal Info
    - Settings | Administration | Basic Settings (https://www.techrepublic.com/article/how-to-configure-smtp-for-nextcloud/)
1. Administration | Security: Check password against the list of breached passwords...
1. Setup Onlyoffice
    - Install app
    - Go into Adminstration ONLYOFFICE and set URL and key you set in `.env` file
    - Enable:
        - File extensions for the stuff you want
        - Disable Chat
1. Upload Logo, change theme/background etc

## Backup

Backup is carried out using Restic. Best practices should use the 3-2-1 principle - (3 copies of the data, 2 backups, 1 off-site). This only provides 1 backup so it is recommended that you also regularly make a copy of the S3 backup location.

1. Set up an S3 account eg with Digital Ocean or Backblaze
1. Copy the `.restic-env-example` to `.restic-env` and fill out the correct values
1. `chmod +x ./backup.sh` to make it directly executable
1. Run `./backup.sh` - it should set up the backup repo and do an initial backup
1. Setup crontab to run this regularly - eg every 24h:
    - run `crontab -e` to edit crontab file
    - add this line `0 0 * * * /root/nextcloud/backup.sh`
    - verify it's set using `crontab -l`
    - you can confirm your backups by running `bash ./snapshots.sh`

### Debugging backups
The backup script logs each backup to the `./backups` subdirectory as well as the console.

In all log outputs other than your first you will see the line:

```
Fatal: create key in repository at s3:{your-s3-bucket-location} failed: repository master key and config already initialized
```

This happens because the script tries to create the repository each time, before backing up, and after the first time it's already happened. I cleaner solution would be to check for the existence of the repository before attempting to create it but I haven't found an easy way to do it. PRs welcome :-)

### Debugging crontab

As per https://stackoverflow.com/questions/4883069/debugging-crontab-jobs:

- uncomment `cron.*` line in `/etc/rsyslog.conf` or `/etc/rsyslog.d/50-default.conf` (ubuntu)
- then restart cron and rsyslog:

```bash
sudo service rsyslog restart
sudo service cron restart
```

Cron logs will now go to:
`/var/log/cron.log`
