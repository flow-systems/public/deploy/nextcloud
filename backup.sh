#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

printf "Running backup script to backup Nextcloud\n"
echo "-----------------------------------------\n"

# Log to console and logfile
mkdir -p ${SCRIPT_DIR}/backuplogs;
LOG_FILE=${SCRIPT_DIR}/backuplogs/$(date -d "today" +"%Y%m%d%H%M").log
echo "Logging to file ${LOG_FILE}..."
exec > >(tee $LOG_FILE) 2>&1

# Get environment variables from external files
echo "Getting environment variables..."
export $(grep -v '^#' ${SCRIPT_DIR}/.env | xargs)
export $(grep -v '^#' ${SCRIPT_DIR}/.restic-env | xargs)

# Set default values
if [ -z ${NEXTCLOUD_DB_DATABASE+x} ]; then NEXTCLOUD_DB_DATABASE="nextcloud";fi
if [ -z ${NEXTCLOUD_DB_USER+x} ]; then NEXTCLOUD_DB_USER="nextcloud";fi
if [ -z ${NEXTCLOUD_FOLDER+x} ]; then NEXTCLOUD_FOLDER="./www/nextcloud";fi

# Set remaining environment variables
PATH=PATH=$PATH:/usr/bin/:/usr/local/bin # ensure this works when run from cron
DB_DUMP_PATH=/temp
CONFIG_PATH="${SCRIPT_DIR}"/"${NEXTCLOUD_FOLDER}"/config
DATA_PATH=${SCRIPT_DIR}/${NEXTCLOUD_FOLDER}/data
THEMES_PATH=${SCRIPT_DIR}/${NEXTCLOUD_FOLDER}/themes

# Echo the env vars
printf "NEXTCLOUD_FOLDER: %s\n" $NEXTCLOUD_FOLDER
printf "DB_DUMP_PATH: %s\n" $DB_DUMP_PATH
printf "CONFIG_PATH: %s\n" $CONFIG_PATH
printf "DATA_PATH: %s\n" $DATA_PATH
printf "THEMES_PATH: %s\n" $THEMES_PATH
printf "NEXTCLOUD_DB_DATABASE: %s\n" $NEXTCLOUD_DB_DATABASE
printf "NEXTCLOUD_DB_USER: %s\n" $NEXTCLOUD_DB_USER
if [ -z ${NEXTCLOUD_DB_PASSWORD+x} ]; then echo "NEXTCLOUD_DB_PASSWORD not set";fi

printf "Putting Nextcloud into maintenance mode...\n"
docker-compose -f ${SCRIPT_DIR}/docker-compose.yml exec -T -u www-data app php occ maintenance:mode --on

printf "Dumping the current contents of the database...\n"
mkdir -p ${DB_DUMP_PATH};
docker-compose -f ${SCRIPT_DIR}/docker-compose.yml exec -T db mysqldump --single-transaction --user="$NEXTCLOUD_DB_USER" --password="$NEXTCLOUD_DB_PASSWORD" "$NEXTCLOUD_DB_DATABASE" > ${DB_DUMP_PATH}/nextcloud-sqlbkp.bak

printf "Ensuring restic repo %s is set up...\n" ${RESTIC_REPOSITORY}
# this only needs to be run once and will fail with no consequences after that
docker run --rm -i \
    -e RESTIC_REPOSITORY=$RESTIC_REPOSITORY \
    -e RESTIC_PASSWORD=$RESTIC_PASSWORD \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    restic/restic init

printf "Backing up to bucket %s...\n" ${RESTIC_REPOSITORY}
docker run --rm -i \
    -v ${DB_DUMP_PATH}:/dbdata \
    -v ${DATA_PATH}:/data \
    -v ${THEMES_PATH}:/themes \
    -v ${CONFIG_PATH}:/config \
    -e RESTIC_REPOSITORY=$RESTIC_REPOSITORY \
    -e RESTIC_PASSWORD=$RESTIC_PASSWORD \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    restic/restic backup /dbdata /themes /config /data

printf "Taking Nextcloud out of maintenance mode...\n"
docker-compose  -f ${SCRIPT_DIR}/docker-compose.yml exec -T -u www-data app php occ maintenance:mode --off

printf "Deleting temporary files...\n"
rm -rf "${DB_DUMP_PATH}"

printf "Checking state of repo %s...\n" ${RESTIC_REPOSITORY}
# this only needs to be run once and will fail with no consequences after that
docker run --rm -i \
    -e RESTIC_REPOSITORY=$RESTIC_REPOSITORY \
    -e RESTIC_PASSWORD=$RESTIC_PASSWORD \
    -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
    -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
    restic/restic check



printf "Nextcloud backup complete.\n"
