#!/bin/bash

printf "Starting Nextcloud and Onlyoffice...\n"
docker-compose up -d

printf "\nNextcloud and Onlyoffice now running.\n"
